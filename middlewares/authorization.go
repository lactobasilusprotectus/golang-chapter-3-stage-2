package middlewares

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"go-secure-2/databases"
	"go-secure-2/models"
	"net/http"
	"strconv"
)

func ProductAuthorization() gin.HandlerFunc {
	return func(context *gin.Context) {
		db := databases.GetDB()

		id, err := strconv.Atoi(context.Param("id"))

		if err != nil {
			context.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"error":   "Bad request",
				"message": err.Error(),
			})
		}

		var product models.Product
		userData := context.MustGet("userData").(jwt.MapClaims)
		userID := uint(userData["id"].(float64))

		if err := db.Debug().Where("id = ?", id).First(&product).Error; err != nil {
			context.AbortWithStatusJSON(http.StatusNotFound, gin.H{
				"error":   "Not found",
				"message": err.Error(),
			})
			return
		}

		if product.UserID != userID {
			context.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"error":   "Unauthorized",
				"message": "You are not authorized to access this product",
			})
			return
		}

		context.Next()
	}
}
