package models

import (
	"github.com/asaskevich/govalidator"
	"go-secure-2/helpers"
	"gorm.io/gorm"
)

type User struct {
	GormModel
	FullName string    `json:"full_name" gorm:"not null" form:"full_name" valid:"required~Full name is required"`
	Email    string    `json:"email" gorm:"not null;unique" form:"email" valid:"required~Email is required,email~Email is not valid"`
	Password string    `json:"password" gorm:"not null" form:"password" valid:"required~Password is required"`
	Products []Product `json:"-" gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;" json:"products"`
}

func (u *User) BeforeCreate(tx *gorm.DB) (err error) {
	_, err = govalidator.ValidateStruct(u)

	if err != nil {
		return err
	}

	hashPassword, err := helpers.HashPassword(u.Password)

	if err != nil {
		return err
	}

	u.Password = hashPassword

	return nil
}
