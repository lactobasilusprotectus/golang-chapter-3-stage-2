package middlewares

import (
	"github.com/gin-gonic/gin"
	"go-secure-2/helpers"
)

func Authentication() gin.HandlerFunc {
	return func(context *gin.Context) {
		token, err := helpers.ValidateToken(context)

		if err != nil {
			context.AbortWithStatusJSON(401, gin.H{
				"error":   "Unauthorized",
				"message": err.Error(),
			})
			return
		}

		context.Set("userData", token)

		context.Next()
	}
}
