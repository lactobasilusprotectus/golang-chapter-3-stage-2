package models

import (
	"github.com/asaskevich/govalidator"
	"gorm.io/gorm"
)

type Product struct {
	GormModel
	Title       string `json:"title" gorm:"not null" form:"title" valid:"required~Title is required"`
	Description string `json:"description" gorm:"not null" form:"description" valid:"required~Description is required"`
	UserID      uint
	User        *User
}

func (p *Product) BeforeCreate(tx *gorm.DB) (err error) {
	_, err = govalidator.ValidateStruct(p)

	if err != nil {
		return err
	}

	return nil
}

func (p *Product) BeforeUpdate(tx *gorm.DB) (err error) {
	_, err = govalidator.ValidateStruct(p)

	if err != nil {
		return err
	}

	return nil
}
