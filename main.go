package main

import (
	"go-secure-2/databases"
	"go-secure-2/router"
	"log"
)

func main() {
	databases.Start()
	r := router.StartApp()
	err := r.Run(":8080")
	if err != nil {
		log.Fatalf("Error running server: %v", err)
		return
	}
}
