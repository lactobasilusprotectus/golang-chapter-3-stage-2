package controllers

import (
	"github.com/gin-gonic/gin"
	"go-secure-2/databases"
	"go-secure-2/helpers"
	"go-secure-2/models"
)

func UserRegistrasion(c *gin.Context) {
	db := databases.GetDB()

	var user models.User

	switch helpers.GetContentType(c) {
	case "application/json":
		if err := c.ShouldBindJSON(&user); err != nil {
			c.JSON(400, gin.H{
				"error":   "Bad request",
				"message": err.Error(),
			})
			return
		}
	default:
		if err := c.ShouldBind(&user); err != nil {
			c.JSON(400, gin.H{
				"error":   "Bad request",
				"message": err.Error(),
			})
			return
		}
	}

	if err := db.Debug().Create(&user).Error; err != nil {
		c.JSON(400, gin.H{
			"error":   "Bad request",
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, gin.H{
		"message": "User created",
		"user": struct {
			ID       uint   `json:"id"`
			FullName string `json:"full_name"`
			Email    string `json:"email"`
		}{
			ID:       user.ID,
			FullName: user.FullName,
			Email:    user.Email,
		},
	})
}

func Login(c *gin.Context) {
	db := databases.GetDB()

	var user models.User

	switch helpers.GetContentType(c) {
	case "application/json":
		if err := c.ShouldBindJSON(&user); err != nil {
			c.JSON(400, gin.H{
				"error":   "Bad request",
				"message": err.Error(),
			})
			return
		}
	default:
		if err := c.ShouldBind(&user); err != nil {
			c.JSON(400, gin.H{
				"error":   "Bad request",
				"message": err.Error(),
			})
			return
		}
	}

	var userDB models.User

	if err := db.Debug().Where("email = ?", user.Email).First(&userDB).Error; err != nil {
		c.JSON(400, gin.H{
			"error":   "Bad request",
			"message": err.Error(),
		})
		return
	}

	if !helpers.CheckPasswordHash([]byte(user.Password), []byte(userDB.Password)) {
		c.JSON(400, gin.H{
			"error":   "Bad request",
			"message": "Invalid password/email",
		})
		return
	}

	token, _ := helpers.GenerateToken(userDB.ID, userDB.Email)

	c.JSON(200, gin.H{
		"message": "Login success",
		"token":   token,
		"user": struct {
			ID       uint   `json:"id"`
			FullName string `json:"full_name"`
			Email    string `json:"email"`
		}{
			ID:       userDB.ID,
			FullName: userDB.FullName,
			Email:    userDB.Email,
		},
	})
}
