package helpers

import "github.com/gin-gonic/gin"

func GetContentType(x *gin.Context) string {
	return x.Request.Header.Get("Content-Type")
}
