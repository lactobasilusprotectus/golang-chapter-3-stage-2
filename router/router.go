package router

import (
	"github.com/gin-gonic/gin"
	"go-secure-2/controllers"
	"go-secure-2/middlewares"
)

func StartApp() *gin.Engine {
	router := gin.Default()

	//users routes
	userRouter := router.Group("/users")
	{
		userRouter.POST("/register", controllers.UserRegistrasion)
		userRouter.POST("/login", controllers.Login)
	}

	//products routes
	productRouter := router.Group("/products")
	{
		productRouter.Use(middlewares.Authentication())
		productRouter.POST("/", controllers.CreateProduct)
		productRouter.GET("/:id", middlewares.ProductAuthorization(), controllers.GetProducts)
		productRouter.PUT("/:id", middlewares.ProductAuthorization(), controllers.UpdateProduct)
		productRouter.DELETE("/:id", middlewares.ProductAuthorization(), controllers.DeleteProduct)
	}

	return router
}
