package controllers

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"go-secure-2/databases"
	"go-secure-2/helpers"
	"go-secure-2/models"
	"net/http"
	"strconv"
)

func CreateProduct(c *gin.Context) {
	db := databases.GetDB()
	userData := c.MustGet("userData").(jwt.MapClaims)

	var product models.Product
	userID := uint(userData["id"].(float64))

	switch helpers.GetContentType(c) {
	case "application/json":
		if err := c.ShouldBindJSON(&product); err != nil {
			c.JSON(400, gin.H{
				"error":   "Bad request",
				"message": err.Error(),
			})
			return
		}
	default:
		if err := c.ShouldBind(&product); err != nil {
			c.JSON(400, gin.H{
				"error":   "Bad request",
				"message": err.Error(),
			})
			return
		}
	}

	product.UserID = userID

	if err := db.Debug().Create(&product).Error; err != nil {
		c.JSON(400, gin.H{
			"error":   "Bad request",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusCreated, gin.H{
		"message": "Product created",
		"product": product,
	})
}

func GetProducts(c *gin.Context) {
	db := databases.GetDB()

	var products []models.Product

	if err := db.Debug().Find(&products).Error; err != nil {
		c.JSON(400, gin.H{
			"error":   "Bad request",
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, gin.H{
		"message":  "Products found",
		"products": products,
	})
}

func UpdateProduct(c *gin.Context) {
	db := databases.GetDB()

	id, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		c.JSON(400, gin.H{
			"error":   "Bad request",
			"message": err.Error(),
		})
		return
	}

	var product models.Product

	if err := db.Debug().Where("id = ?", id).First(&product).Error; err != nil {
		c.JSON(404, gin.H{
			"error":   "Not found",
			"message": err.Error(),
		})
		return
	}

	switch helpers.GetContentType(c) {
	case "application/json":
		if err := c.ShouldBindJSON(&product); err != nil {
			c.JSON(400, gin.H{
				"error":   "Bad request",
				"message": err.Error(),
			})
			return
		}
	default:
		if err := c.ShouldBind(&product); err != nil {
			c.JSON(400, gin.H{
				"error":   "Bad request",
				"message": err.Error(),
			})
			return
		}
	}

	if err := db.Debug().Save(&product).Error; err != nil {
		c.JSON(400, gin.H{
			"error":   "Bad request",
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, gin.H{
		"message": "Product updated",
		"product": product,
	})
}

func DeleteProduct(c *gin.Context) {
	db := databases.GetDB()

	id, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		c.JSON(400, gin.H{
			"error":   "Bad request",
			"message": err.Error(),
		})
		return
	}

	var product models.Product

	if err := db.Debug().Where("id = ?", id).First(&product).Error; err != nil {
		c.JSON(404, gin.H{
			"error":   "Not found",
			"message": err.Error(),
		})
		return
	}

	if err := db.Debug().Delete(&product).Error; err != nil {
		c.JSON(400, gin.H{
			"error":   "Bad request",
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, gin.H{
		"message": "Product deleted",
	})
}
